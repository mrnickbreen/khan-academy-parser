# README #

This parser is my assignment for an interview with Khan Academy.
I use 'we' throughout this readme to refer to the project as a whole (especially as it relates
to it's future (nonexistant) development).

The index website is a basic code classroom with sample code already written to
display some functionality. Index analyzes student-generated javascript code.
The student's teacher sets up a whitelist, a blacklist, and a rough structure of
the program ahead of time. As the student types it verifies:
* The student uses all elements appearing in the whitelist
* The student does not use any elements appearing in the blacklist
* The student's program matches the rough structure of the code provided by the teacher.

It uses the student-parser.js API functions:
* verifyWhitelist
* verifyBlacklist
* verifyProgramStructure

### How do I get set up? ###

* Clone repo and open up index.html in any browser (IE8+)
* API (and documentation) is found in student-parser.js
* Notice pre-set whitelist, blacklist, and suggestedCodeStructure in index.js
feel free to modify.

### Notes on Decisions and Choices  ###

## Why I chose Esprima over Acorn ##
* Esprima's author made phantomJS (headless browser testing env for JS testing)

* Esprima has a BSD license (good for use at Khan Academy)
* It has 100% test coverage (https://travis-ci.org/jquery/esprima/jobs/113430247) and more tests than acorn,
* Esprima has double the amount of github stars, watchers, and forks
* Esprima's supported by the JQUERY foundation/org
* Esprima runs on IE8+ browsers
* By Acorn's author's own admission, Esprima is well documented
* Esprima has a lot more tooling than just parsing, when Khan Academy needs
additional tooling, we can easily use other parts of the Esprima library.

Acorn was originally amazing because it was so much faster than Esprima, however,
Esprima is gaining ground and becoming much faster. (50% without location and 85% with location data)

## Why I didn't use JQUERY ##
Wasn't strictly needed and it was bloat/loading speed I didn't want to take on.

## Precautions I took to support IE8 ##
Using underscore should take care of IE8's lack of support of hasOwnProperty,
which I use frequently in this code. I also confirmed Esprima works in IE8+

## How I'd make textarea calls not blocking (hint: html5 web workers) ##
First and foremost, IE8 and IE9 do not support webworkers, so before I'd
try and implement web workers I'd write checks to confirm we're not on IE.

The specification suggested I make sure the 'tests are run in a non-blocking way'
and the best way I can think of to do this is using web workers. Web workers help
run javascript in the background, thus, not affecting the performance of the page.

I'd wrap some of the logic in index.js into a webworker file and call that
webworker when the textarea's keydown event is fired. Note, I'd also have to
pass the webworker the code the student has typed by posting a message to the
webworker. (More info here: http://stackoverflow.com/questions/4019230/javascript-web-workers-how-do-i-pass-arguments)

## Considerations for using this in production ##
I thought about why we're supporting IE8 and education software
supports outdated browers because it's students can't afford to upgrade (
very slow internet speeds, can't afford the bandwidth on large / frequent browser updates, etc).
Luckily, most of this app is done client side, so we're reducing our bandwidth footprint.

We'd want to move most of this logic to a backend if we're worried about
users tampering with the validity of the verification. For example, if these tests
were used for any kind of validation or degree credits we wouldn't want the
student to interfere or see the teacher's solution.

Also, we're using trivial code examples in index.html. If we we're to analyze
and parse much larger code samples we'd want to more that logic to the server
to take advantage of faster analyzing & parsing times.

## 3rd API requirement discussion ##
The Interview document suggested the following: "Determine the rough structure of the program. For example, "There should be a 'for loop' and inside of it there should be an 'if statement'.""

I understood this as the teacher providing a code sample that should be used
as a comparison for the student's piece of code. It would have to match in general,
but if the student uses different variable names that should be okay. However, if
the student added a bunch of other code (while loop for example), the comparison
would fail.

As an additional addon we can add another parameter for verifyProgramStructure to
do sub-tree comparison. For example, if the teacher wants to verify
the student's code contains an ifstatement with a variable assignment,
both student code would pass:
if (1===1) {
    test = 'meow';
}

or

var test = 'moo';
if (1===1) {
    test = 'meow';
}

This sub-tree comparison functionality makes the logic much more interesting and
fun, but seemed beyond the scope of what you were asking for. You can take at
past commits if you're interested in seeing my attempt, it was traversing
through each node in the student code AST and comparing to the teacher code AST.

Lastly, If we want our teacher-student-code comparison method to be more strict,
we can enhance currentNodeIsEqual to do more than type checking.

## FUTURE TODOS ##
* Additional parameter on the verifyProgramStructure to do comparisons on
variable names or values, since it's currently, purposely, just type checking.
* Add more ESPIRMA_TYPES's to index.js to help teachers understand full capability
of the student-parser.js API.
* Implement html5 web workers
* implement cooler coding area than a html text-area
* Conform closer to style guide provided by Khan Academy
https://github.com/Khan/style-guides (I tried to do this as I coded, but I'm
sure I broke a few rules)

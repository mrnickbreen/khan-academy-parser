var StudentParser = {
    _occurrenceOfSpecificFunctionalityInCode: {},

    /**
     * Determines if javascript code contains specific whitelisted functionality.
     * If the code string is invalid the method returns false and sends
     * a message to console.debug.
     * @param  {string} code      a string representation of javascript code
     * @param  {object} whiteList an array of string respresenting
     * specific functionality that should exist in the code. Uses ESPIRMA.js'
     * naming conventions for the speicific functionality.
     * @return {boolean} Returns true if all the specific functionality
     * (ex: if statements) on the whitelist is included at least once
     * in the code.
     */
    verifyWhitelist: function(code, whiteList) {
        var i = 0;
        var currentWhitelistWord;
        var currentWhitelistWordNotInStats;

        // Confirming variables are correct type, type-checking.
        if (typeof code != 'string' || typeof whiteList != 'object') {
            console.debug('verifyWhitelist\'s parameters were the wrong type');
            return false;
        }

        try {
            this._analyzeCode(code);
        } catch(err) {
            console.debug('code passed to verifyWhitelist is not valid');
            return false;
        }

        for (i=0; i < whiteList.length; i++) {
            currentWhitelistWord = whiteList[i];
            currentWhitelistWordInStats =
                _.has(this._occurrenceOfSpecificFunctionalityInCode,
                    currentWhitelistWord);
            if (!currentWhitelistWordInStats) {
                return false;
            }
        }
        return true;
    },

     /**
      * Determines if javascript code contains specific blacklisted
      * functionality.
      * If the code string is invalid the method returns false and sends
      * a message to console.debug.
      * @param  {string} code      a string representation of javascript code
      * @param  {object} blackList an array of string respresenting
      * specific functionality that should not exist in the code.
      * Uses ESPIRMA.js' naming conventions for the speicific functionality.
      * @return {boolean} Returns true if the code contains no specific
      * functionality (ex: if statements) on the blacklist.
      */
    verifyBlacklist: function(code, blackList) {
        var i, currentBlacklistWord, isBlacklistWordInStats;

        // Confirming variables are correct type, type-checking.
        if (typeof code != 'string' || typeof blackList != 'object') {
            console.debug('verifyBlacklist\'s parameters were the wrong type');
            return false;
        }

        try {
            this._analyzeCode(code);
        } catch(err) {
            console.debug('code passed to verifyBlacklist is not valid');
            return false;
        }
        for (i=0; i < blackList.length; i++) {
            currentBlacklistWord = blackList[i];
            isBlacklistWordInStats =
                _.has(this._occurrenceOfSpecificFunctionalityInCode,
                    currentBlacklistWord);
            if (isBlacklistWordInStats) {
                return false;
            }
        }
        return true;
    },

    /**
     * Verifies one javascript program's code structure against another
     * program's code structure.
     * NOTE: Only does rudimentary type comparisons, allows differences
     * in variable names and the values that are assigned to them. Does NOT
     * do subtree matching (on purpose).
     * If either code string is invalid the method returns false and sends
     * a message to console.debug.
     *
     * @param  {string} studentCode a string representation of javascript code
     * @param  {string} teacherCode a string representation of javascript code
     * @return {boolean}             returns true if both program structures
     * match.
     */
    verifyProgramStructure: function(studentCode, teacherCode) {
        var _this = this;
        try {
            var studentCodeEsprimaOutput = esprima.parse(studentCode);
            var teacherCodeEsprimaOutput = esprima.parse(teacherCode);
        } catch(err) {
            console.debug('code passed to verifyProgramStructure is not valid');
            return false;
        }

        // Confirming variables are correct type, type-checking.
        if (typeof studentCode != 'string' || typeof teacherCode != 'string') {
            console.debug('verifyProgramStructure\'s parameters were' +
                ' the wrong type');
            return false;
        }

        return this._isEqualTree(studentCodeEsprimaOutput,
            teacherCodeEsprimaOutput);
    },

    /**
     * Analyzes the output from parsing the code using esprima's parse.
     * It determines how many times specific functionality
     * (ex: if statement) is used in the code sample.
     * Using http://sevinf.github.io/blog/2012/09/29/esprima-tutorial/
     * as reference
     * @param  {String} code a string representation of javascript code
     */
    _analyzeCode: function(code) {
        var _this = this;
        var updateCount = function(type) {
            if (!_this._occurrenceOfSpecificFunctionalityInCode[type]) {
                _this._occurrenceOfSpecificFunctionalityInCode[type] = {calls: 1};
            } else {
                _this._occurrenceOfSpecificFunctionalityInCode[type].calls++;
            }
        };
        // Resetting _occurrenceOfSpecificFunctionalityInCode
        _this._occurrenceOfSpecificFunctionalityInCode = {};
        esprimaOutput = esprima.parse(code);

        this._traverse(esprimaOutput, function(node) {
            updateCount(node.type);
        });
    },

    /**
     * Traverses through an Abstract Syntax Tree (AST) in a breadth first
     * search order.
     * @param  {Object} node A node in the AST repesenting a line of code
     * in the program that's being parsed
     * @param  {function} func This is called every time we traverse
     */
    _traverse: function(node, func) {
        func(node);
        var _this = this;
        for (var key in node) {
            if (_.has(node, key)) {
                var child = node[key];
                if (typeof child === 'object' && child !== null) {

                    if (Array.isArray(child)) {
                        child.forEach(function(node) {
                            _this._traverse(node, func);
                        });
                    } else {
                        _this._traverse(child, func);
                    }
                }
            }
        }
    },

    /**
     * [function description]
     * @param  {object} tree        Abstract Syntax Tree (AST)
     * @param  {object} treeToMatch Abstract Syntax Tree (AST)
     * @return {boolean}            Returns true if both trees' structures
     * are the same. Only does rudimentary type comparisons, allows for
     * differences in variable names and the values that are assigned to them
     */
    _isEqualTree: function(tree, treeToMatch) {
        var bothNodesHaveSameNumberOfChildren;
        var treeChildren;
        var treeToMatchChildren;
        var i;
        if (this._currentNodeIsEqual(tree, treeToMatch)) {
            // Determine if both nodes have same number of children and
            // save children groups to local variables
            if (_.has(tree, 'consequent')) {
                // If statements have consequent and body
                bothNodesHaveSameNumberOfChildren =
                    tree.consequent.body.length ===
                        treeToMatch.consequent.body.length;
                treeChildren = tree.consequent.body;
                treeToMatchChildren = treeToMatch.consequent.body;
            } else if (_.has(tree, 'body')) {
                // While and root nodes have body properties
                bothNodesHaveSameNumberOfChildren = tree.body.length ===
                    treeToMatch.body.length;
                treeChildren = tree.body;
                treeToMatchChildren = treeToMatch.body;
            } else {
                // There are no children, and since the current node is
                // equal, this tree is equal.
                return true;
            }

            if (bothNodesHaveSameNumberOfChildren) {
                // For each child group, recurse on the function to
                // verify each child's subtrees are equal to the matching
                // pattern's child subtrees
                for (i = 0; i < treeChildren.length; i++) {
                    if (!(this._isEqualTree(treeChildren[i],
                        treeToMatchChildren[i]))) {
                            return false;
                    }
                }
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    },

    /**
     * Determines if two AST nodes have the same type.
     * @param  {Object} nodeA
     * @param  {Object} nodeB
     * @return {boolean}       true if node types are the same
     */
     _currentNodeIsEqual: function(nodeA, nodeB) {
        var bothAreRootNodes = nodeA.type === 'Program' &&
            nodeB.type === 'Program';
        var bothNodesHaveSameType = nodeA.type === nodeB.type;
        return bothAreRootNodes || bothNodesHaveSameType;
    }
};

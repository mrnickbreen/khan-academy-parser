// Wrapping everything to not leak into global namespace
(function() {
    var textAreaId = 'code-text-area',
        textAreaDom = document.getElementById(textAreaId),
        whiteListSuccessId = 'whitelist-success',
        blackListSuccessId = 'blacklist-success',
        whiteListFailureId = 'whitelist-failure',
        blackListFailureId = 'blacklist-failure',
        resultsLoadingId = 'results-loading',
        suggestedStructureSuccessId = 'suggested-structure-success',
        suggestedStructureFailureId = 'suggested-structure-failure',
        ESPIRMA_TYPES = {
            IfStatement: 'IfStatement',
            ExpressionStatement: 'ExpressionStatement',
            VariableDeclaration: 'VariableDeclaration',
            VariableDeclarator: 'VariableDeclarator',
            WhileStatement: 'WhileStatement',
            AssignmentExpression: 'AssignmentExpression'
        },
        whiteList = [ESPIRMA_TYPES.IfStatement, ESPIRMA_TYPES.ExpressionStatement],
        blackList = [ESPIRMA_TYPES.WhileStatement],
        suggestedCodeStructure = 'var cow = \'moo\';'+
            'if (cow === \'moo\') {'+
                'cow = \'squeel\';'+
            '}'+
            'while (cow === \'squeel\') {'+
                'test = \'bark\';'+
            '}',

        runParse = function() {
            var codeToParse = document.getElementById(textAreaId).value;
            var verifyWhiteListResult = StudentParser.verifyWhitelist(codeToParse, whiteList);
            var verifyBlackListResult = StudentParser.verifyBlacklist(codeToParse, blackList);
            var verifyProgramStructureResult = StudentParser.verifyProgramStructure(codeToParse,
                suggestedCodeStructure);

            document.getElementById(resultsLoadingId).style.display = 'none';
            if (verifyWhiteListResult) {
                document.getElementById(whiteListSuccessId).style.display =
                    'block';
            } else {
                document.getElementById(whiteListFailureId).style.display =
                    'block';
            }

            if (verifyBlackListResult) {
                document.getElementById(blackListSuccessId).style.display =
                    'block';
            } else {
                document.getElementById(blackListFailureId).style.display =
                    'block';
            }

            if (verifyProgramStructureResult) {
                document.getElementById(suggestedStructureSuccessId).style.display =
                    'block';
            } else {
                document.getElementById(suggestedStructureFailureId).style.display =
                    'block';
            }
        },
        displayLoadingMessage = function() {
            document.getElementById(resultsLoadingId).style.display = 'block';
            document.getElementById(whiteListSuccessId).style.display = 'none';
            document.getElementById(blackListSuccessId).style.display = 'none';
            document.getElementById(whiteListFailureId).style.display = 'none';
            document.getElementById(blackListFailureId).style.display = 'none';
            document.getElementById(suggestedStructureSuccessId).style.display = 'none';
            document.getElementById(suggestedStructureFailureId).style.display = 'none';
        };

    textAreaDom.addEventListener('keydown', function() {
        displayLoadingMessage();
        delay(function(){
            runParse();
        }, 1000 );
    }, false);

    // A bit of logic to make an educated guess that the student has
    // stopped typing. Helps reduce unnecessary calls to API.
    // From http://stackoverflow.com/questions/1909441/jquery-keyup-delay
    var delay = (function(){
      var timer = 0;
      return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
      };
    })();

    // Updating div's to have content
    document.getElementById('whitelist-contents').innerText =
        whiteList.join(', ');
    document.getElementById('blacklist-contents').innerText =
        blackList.join(', ');

    // On load we'll run the runParse to show student what this code is doing.
    runParse();

}());
